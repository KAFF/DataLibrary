+++
Title = 'Game stores'
Tags = ['POIs', 'Shops']

Provider = 'Game'
Description = 'POI dataset of Game stores'
CopyrightHolder = 'Game'
CopyrightNotice = 'First party data from Game, use with caution.'
InLanguage = 'en-GB'
+++


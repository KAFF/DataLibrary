+++
Title = 'Apex Hotels locations'
Tags = ['POIs', 'Hotels']

Provider = 'Apex Hotels'
Description = 'POI dataset of Apex Hotels locations'
CopyrightHolder = 'Apex Hotels'
CopyrightNotice = 'First party data from Apex Hotels, use with caution.'
InLanguage = 'en-GB'
+++


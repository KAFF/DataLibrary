+++
Title = 'Walgreens stores'
Tags = ['POIs', 'Shops']

Provider = 'Walgreens'
Description = 'POI dataset of Walgreens stores'
CopyrightHolder = 'Walgreens'
CopyrightNotice = 'First party data from Walgreens, use with caution.'
InLanguage = 'en-GB'
+++


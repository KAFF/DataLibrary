+++
Title = 'Hidden Hearing stores'
Tags = ['POIs', 'Shops']

Provider = 'Hidden Hearing'
Description = 'POI dataset of Hidden Hearing stores'
CopyrightHolder = 'Hidden Hearing'
CopyrightNotice = 'First party data from Hidden Hearing, use with caution.'
InLanguage = 'en-GB'
+++

